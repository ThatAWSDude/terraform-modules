locals {
  map_users = {
    user_arn = "arn:aws:iam::083625559372:user/gary.shaw"
    username = "gary.shaw"
    group    = "system:masters"
  }

  worker_group = {
    instance_type = "t3.small"
    spot_price    = "0.03"
  }
}

module "eks" {
  source = "../../"

  cluster_name    = var.cluster_name
  map_users       = [local.map_users]
  use_default_vpc = true
  worker_groups   = [local.worker_group]
}
