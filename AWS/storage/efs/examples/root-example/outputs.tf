output "efs_arn" {
  description = "The Amazon Resource Name of the file system"
  value       = module.efs.arn
}

output "efs_id" {
  description = "The Amazon Resource Name of the file system"
  value       = module.efs.id
}