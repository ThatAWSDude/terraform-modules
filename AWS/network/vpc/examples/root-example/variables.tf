variable "region" {
  description = "AWS Region"
  type        = string
  default     = "eu-west-2"
}

variable "vpc_name" {
  description = "VPC Name"
  type        = string
  default     = "root-example"
}