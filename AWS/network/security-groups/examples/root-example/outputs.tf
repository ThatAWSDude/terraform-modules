output "security_group_id" {
  description = "ID of the security group that has been created"
  value       = module.sg.security_group_id
}