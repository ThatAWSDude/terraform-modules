output "security_group_id" {
  description = "ID of the security group that has been created"
  value       = module.this.this_security_group_id
}