
variable "name_prefix" {
  description = "Name prefix for all resources"
  type        = string
  default     = "root-example"
}