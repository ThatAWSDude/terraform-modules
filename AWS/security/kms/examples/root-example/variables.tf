variable "alias_name" {
  description = "Name of key alias"
  type        = string
  default     = "renovite-key"
}