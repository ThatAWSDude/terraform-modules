variable "domain_name" {
  description = "Domain name for certificate"
  type        = string
  default     = "devops.renovite.cloud"
}